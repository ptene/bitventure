﻿
CREATE TABLE [dbo].[Account]
(
	[Id] INT NOT NULL PRIMARY KEY, 
    [AccountHolder] NVARCHAR(250) NOT NULL, 
    [BranchCode] NVARCHAR(250) NOT NULL, 
    [AccountType] INT NOT NULL, 
    [TransactionDate] DATETIME NOT NULL, 
    [Amount] DECIMAL(18, 2) NOT NULL, 
    [Status] NVARCHAR(50) NOT NULL, 
    [EffectiveStatusDate] DATETIME NOT NULL, 
    [AccountNumber] NVARCHAR(250) NOT NULL
)
