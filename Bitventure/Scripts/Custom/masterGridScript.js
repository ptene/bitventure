﻿
function editRecord(node) {

    var nodeId = $(node).attr('id');
    clearEditing();
    
    //Add a class to all cells that are edited
    $(node).addClass('editableCell');
    $(node).find('span').addClass('d-none');

    //Created a dropdown to append to the edited cell
    var html = '<div id="selectAccType" class="text-center">' +
                '<select name="AccountType" id="accType_' + nodeId + '">' +
                '<option value="1">Current/Cheque</option>' +
                '<option value="2">Savings</option>' +
        '</select><span id="loader" class="d-none"><img src="~/Images/loader.gif" /></span>' +
        '<button id="save" class="btn btn-primary btn-sm" type="button" onClick="saveRecord(' + nodeId + ')" style="margin: auto 5px auto auto">Save</button>' +
        '<button id="cancel" class="btn btn-danger btn-sm" type="button" onClick="clearEditing()">Cancel</button></div > ';

    $(node).addClass("cellEditing");
    $(node).append(html);
    $(node).children().first().focus();
}


function saveRecord(nodeId)
{

    var accountType = $('#accType_' + nodeId).val();
    var node = "td#" + nodeId;

    if (!!accountType)
    {
        $(node + ' > div#selectAccType > span#loader').removeClass('d-none');
        $(node + ' > div#selectAccType > button#save').attr('disabled', 'disabled');
        $(node + ' > div#selectAccType > button#cancel').attr('disabled', 'disabled');

        //Save the edited record
        $.get('/Home/SaveRecord?id=' + nodeId + '&accountType=' + accountType, function (result) {

            $(node + ' > div#selectAccType > span#loader').addClass('d-none');
            $(node + ' > div#selectAccType > button#save').removeAttr('disabled');
            $(node + ' > div#selectAccType > button#cancel').removeAttr('disabled');

            if (!!result) {

                //Update the cell result
                $(node).find('span').html(result);
                $(node).find('span').removeClass('d-none');
                $(node).find('div#selectAccType').remove();
                $(node).removeClass('editableCell');
            }

        });

    }


}

function clearEditing() {
    //Clear all the editing added to the cells
    $('tbody#masterGrid > tr > td.editableCell > div#selectAccType').remove();
    $('tbody#masterGrid > tr > td.editableCell > span').removeClass('d-none');
    $('tbody#masterGrid > tr > td.editableCell').removeClass('editableCell');
}



function showDetails(accountId, id) {

    //highlight the master grid selected row
    $('#masterGrid').children().removeClass('grey');
    $('section#detailGrid > div#loader').removeClass('d-none');

    $(id).addClass('grey');

    if (!!accountId) {
        //Get the updated record
        $.get('/Home/GetAccountDetails?id=' + accountId, function (result) {
            $('section#detailGrid > div#loader').addClass('d-none');

            if (!!result) {
                $('#detailGrid').find("#content").html(result);
            }

        });
    }
}
