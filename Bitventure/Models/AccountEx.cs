﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;

namespace Bitventure.Models
{
    public partial class Account
    {

        public static Account ParseCSVRow(string line)
        {
            string[] entries = line.Split(',');

            Account account = new Account();

            if(entries.Length > 0)
            {
                int id;
                int accountType;
                decimal amount;

                Int32.TryParse(entries[0], out id);
                account.Id = id;
                account.AccountHolder = string.IsNullOrEmpty(entries[1]) ? string.Empty : entries[1];
                account.BranchCode = string.IsNullOrEmpty(entries[2]) ? string.Empty : entries[2];

                decimal accNumber = 0;

                decimal.TryParse(entries[3], NumberStyles.Number | NumberStyles.AllowExponent, CultureInfo.InvariantCulture, out accNumber);
                account.AccountNumber = accNumber == 0 ? string.Empty : accNumber.ToString(CultureInfo.InvariantCulture);




                //account.AccountNumber = string.IsNullOrEmpty(entries[3]) ? string.Empty : entries[3].ToString(CultureInfo.InvariantCulture);
                Int32.TryParse(entries[4], out accountType);
                account.TransactionDate = Convert.ToDateTime(entries[5], System.Globalization.CultureInfo.GetCultureInfo("ur-PK").DateTimeFormat);
                account.AccountType = accountType;
                Decimal.TryParse(entries[6], out amount);
                account.Amount = amount;
                account.Status = string.IsNullOrEmpty(entries[7]) ? string.Empty : entries[7].ToString().PadLeft(2, '0');
                account.EffectiveStatusDate = Convert.ToDateTime(entries[8], System.Globalization.CultureInfo.GetCultureInfo("ur-PK").DateTimeFormat);
            }

            return account;
        }

        public static double GetEffectiveDays(DateTime? transactionDate, DateTime? effectiveDate)
        {
            if(transactionDate.HasValue && effectiveDate.HasValue)
                return Math.Abs((effectiveDate.Value - transactionDate.Value).TotalDays);

            return 0;
        }

        public static string GetStatusName(string status)
        {
            if (!string.IsNullOrEmpty(status) && status.Equals("00"))
                return @"Successful";
            else if (!string.IsNullOrEmpty(status) && status.Equals("30"))
                return @"Disputed";
            else
                return @"Failed";
        }

    }
}