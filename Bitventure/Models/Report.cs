﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Bitventure.Models
{
    public class Report
    {
        public string BranchCode { get; set; }
        public string Status { get; set; }
        public string AccountType { get; set; }
        public int Count { get; set; }
        public decimal Amount { get; set; }
    }
}