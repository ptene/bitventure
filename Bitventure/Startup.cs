﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Bitventure.Startup))]
namespace Bitventure
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
