﻿using Bitventure.Models;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Migrations;
using System.IO;
using System.Linq;
using System.Threading;
using System.Web;
using System.Web.Mvc;

namespace Bitventure.Controllers
{
    public class HomeController : Controller
    {
        DatabaseContext db;
        public HomeController()
        {
            db = new DatabaseContext();
        }

        public ActionResult Index()
        {
            return View();
        }
          
        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        [HttpPost]
        public ActionResult ProcessFile()
        {

            Thread.Sleep(5000);

            

            if(Request.Files.Count > 0)
            {
                try
                {
                    HttpFileCollectionBase files = Request.Files;
                    HttpPostedFileBase file = files[0];

                    string fileExtension = file.FileName.Split('.').Last();
                    string folder = "~/Files/";

                    if (!System.IO.Directory.Exists(Server.MapPath(folder)))
                    {
                        //Create File directory if not exists
                        Directory.CreateDirectory(Server.MapPath(folder));
                    }

                    string filePath = Path.Combine(Server.MapPath(folder), string.Format("{0}.{1}", Guid.NewGuid(), fileExtension));
                    file.SaveAs(filePath);

                    using (DatabaseContext db = new DatabaseContext())
                    {
                        //Read all file content and parse it
                        List<Account> accounts = System.IO.File.ReadAllLines(filePath)
                                                    .Skip(1)
                                                    .Select(x => Account.ParseCSVRow(x))
                                                    .ToList();
                        
                        foreach(var account in accounts)
                        {
                            //Save new data and update existing
                            db.Set<Account>().AddOrUpdate(x => x.Id, account);
                        }

                        try
                        {
                            db.SaveChanges();
                            //delete file if exists
                            if (System.IO.File.Exists(filePath))
                                System.IO.File.Delete(filePath);
                        }
                        catch(Exception ex)
                        {

                        }
                    }

                }
                catch(Exception ex)
                {
                    return null;
                }
            }

            return View("Index");
        }

        [HttpGet]
        public ActionResult GetImports()
        {
            var accounts = db.Accounts.ToList();
            return PartialView("_ImportView", accounts);
        }

        [HttpGet]
        public ActionResult GetReports()
        {
            //query report data
            //
            var report = db.Accounts.GroupBy(x => new { x.BranchCode, x.AccountType, x.Status })
                                  .Select(x => x.FirstOrDefault())
                                  .Select(x => new Report()
                                  {
                                      BranchCode = x.BranchCode,
                                      AccountType = x.AccountType == 1 ? "Current/Cheque" : x.AccountType == 2 ? "Savings" : string.Empty,
                                      Status = !string.IsNullOrEmpty(x.Status) && x.Status.Equals("00") ? "Successful" : !string.IsNullOrEmpty(x.Status) && x.Status.Equals("30") ? "Disputed" : "Failed",
                                      Count = db.Accounts.Where(c => c.BranchCode == x.BranchCode && c.AccountType == x.AccountType && c.Status == x.Status && (x.Status == "00" || x.Status == "30" || (x.Status != "00" && x.Status != "30"))).Count(),
                                      Amount = db.Accounts.Where(c => c.BranchCode == x.BranchCode && c.AccountType == x.AccountType && c.Status == x.Status && (x.Status == "00" || x.Status == "30" || (x.Status != "00" && x.Status != "30"))).Sum(c => c.Amount),
                                  }).ToList();
            
            return PartialView("_ReportView", report);
        }

        [HttpGet]
        public string SaveRecord(int id, int accountType)
        {
            Account account = db.Accounts.Find(id);
            if(account != null)
            {
                account.AccountType = accountType;
                db.SaveChanges();
                return account.AccountType == 1 ? "Current/Cheque" : account.AccountType == 2 ? "Savings" : string.Empty;
            }

            return null;
        }



        [HttpGet]
        public ActionResult GetAccountDetails(int? id)
        {
            if(id.HasValue)
            {
                //only get data for valid id
                var account = db.Accounts.Find(id.Value);
                if(account != null)
                {
                    return PartialView("_DetailGrid", account);
                }
            }


            return null;

            //throw new Exception("Invalid Account");
        }


    }
}